//
//  cristobal.swift
//  navarreteC
//
//  Created by USRDEL on 5/6/18.
//  Copyright © 2018 USRDEL. All rights reserved.
//

import Foundation

struct CristobalInfo: Decodable {
    let list: [CristobalInfo]
}

struct Cristoballista: Decodable {
    let id:Int
    let description:String
}
